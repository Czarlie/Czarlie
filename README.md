# hi, i'm charlie!
[![](https://shields.io/endpoint?url=https%3A%2F%2Fpronouns.cza.li%2Fshield%2Fen)](https://pronouns.cza.li)

<details>
<summary>about me</summary>

i:
  - like python and would start my transpiler into expression-only python again with enough free time
  - like cursed regexes (sorry to the ppl that have to implement pcre)
  - own an adequate amount of blåhajs
  - am somewhat into linguistics and assyriology, but am by no means knowledgeable about either
  - think it's probably a bit weird to have a bulleted list with stuff about me on my profile
  - 
</details>

<details>
<summary>links</summary>

  - [website](https://cza.li), [alternative domain](https://czarlie.me), [another alternative domain](https://arlie.ch)
  - [twitter](https://twitter.com/cza_li)
  - [tumblr (mostly passive)](https://czali.tumblr.com)
  - [my extensions.gnome.org profile](https://extensions.gnome.org/accounts/profile/thecheesegromit) and [the only extension i have on there](https://extensions.gnome.org/extension/3956/gnome-fuzzy-app-search)
  - [github (which i avoid using, usually)](https://github.com/generichttps://extensions.gnome.org/extension/3956/gnome-fuzzy-app-search/username3)
</details>

![hypnosis spiral, the caption reads: ooh you wanna let me merge, you wanna let me merge so bad](https://gitlab.com/Czarlie/Czarlie/-/raw/main/merge.jpg?inline=false "LET ME MERGE")
